<?php

use WpifyWooPluginSkeletonDeps\DI\Definition\Helper\CreateDefinitionHelper;
use WpifyWooPluginSkeletonDeps\Wpify\CustomFields\CustomFields;
use WpifyWooPluginSkeletonDeps\Wpify\Log\RotatingFileLog;
use WpifyWooPluginSkeletonDeps\Wpify\PluginUtils\PluginUtils;

return array(
	CustomFields::class    => ( new CreateDefinitionHelper() )
		->constructor( plugins_url( 'deps/wpify/custom-fields', __FILE__ ) ),
	RotatingFileLog::class => ( new CreateDefinitionHelper() )
		->constructor( 'wpify_woo_plugin_skeleton' ),
	PluginUtils::class     => ( new CreateDefinitionHelper() )
		->constructor( __DIR__ . '/wpify-woo-plugin-skeleton.php' ),
);

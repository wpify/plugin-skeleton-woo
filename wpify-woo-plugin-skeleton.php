<?php
/*
 * Plugin Name:          Wpify Woo Plugin Skeleton
 * Description:          Plugin skeleton with theme by WPify
 * Version:              1.0.0
 * Requires PHP:         7.3.0
 * Requires at least:    5.3.0
 * Author:               WPify
 * Author URI:           https://www.wpify.io/
 * License:              GPL v2 or later
 * License URI:          https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:          wpify-woo-plugin-skeleton
 * Domain Path:          /languages
 * WC requires at least: 4.5
 * WC tested up to:      6.2
*/

use WpifyWooPluginSkeleton\Plugin;
use WpifyWooPluginSkeletonDeps\DI\Container;
use WpifyWooPluginSkeletonDeps\DI\ContainerBuilder;

if ( ! defined( 'WPIFY_WOO_PLUGIN_SKELETON_MIN_PHP_VERSION' ) ) {
	define( 'WPIFY_WOO_PLUGIN_SKELETON_MIN_PHP_VERSION', '7.3.0' );
}

/**
 * @return Plugin
 * @throws Exception
 */
function wpify_woo_plugin_skeleton(): Plugin {
	return wpify_woo_plugin_skeleton_container()->get( Plugin::class );
}

/**
 * @return Container
 * @throws Exception
 */
function wpify_woo_plugin_skeleton_container(): Container {
	static $container;

	if ( empty( $container ) ) {
		$is_production    = ! WP_DEBUG;
		$file_data        = get_file_data( __FILE__, array( 'version' => 'Version' ) );
		$definition       = require_once __DIR__ . '/config.php';
		$containerBuilder = new ContainerBuilder();
		$containerBuilder->addDefinitions( $definition );

		if ( $is_production ) {
			$containerBuilder->enableCompilation( WP_CONTENT_DIR . '/cache/' . dirname( plugin_basename( __FILE__ ) ) . '/' . $file_data['version'], 'WpifyWooPluginSkeletonCompiledContainer' );
		}

		$container = $containerBuilder->build();
	}

	return $container;
}

function wpify_woo_plugin_skeleton_activate( $network_wide ) {
	wpify_woo_plugin_skeleton()->activate( $network_wide );
}

function wpify_woo_plugin_skeleton_deactivate( $network_wide ) {
	wpify_woo_plugin_skeleton()->deactivate( $network_wide );
}

function wpify_woo_plugin_skeleton_uninstall() {
	wpify_woo_plugin_skeleton()->uninstall();
}

function wpify_woo_plugin_skeleton_php_upgrade_notice() {
	$info = get_plugin_data( __FILE__ );

	$string = sprintf(
		__( 'Opps! %s requires a minimum PHP version of %s. Your current version is: %s. Please contact your host to upgrade.', 'wpify-woo-plugin-skeleton' ),
		$info['Name'],
		WPIFY_WOO_PLUGIN_SKELETON_MIN_PHP_VERSION,
		PHP_VERSION
	);
	printf( '<div class="error notice"><p>%s</p></div>', $string);
}

function wpify_woo_plugin_skeleton_php_vendor_missing() {
	$info = get_plugin_data( __FILE__ );

	$string = sprintf(
		__( 'Opps! %s is corrupted it seems, please re-install the plugin.', 'wpify-woo-plugin-skeleton' ),
		$info['Name']
	);
	printf( '<div class="error notice"><p>%s</p></div>', $string);
}

function wpify_woo_plugin_skeleton_woocommerce_not_active() {
	$info = get_plugin_data( __FILE__ );

	$string = sprintf(
		__( 'Plugin %s requires WooCommerce. Please install and activate it first.', 'wpify-woo-plugin-skeleton' ),
		$info['Name']
	);
	printf( '<div class="error notice"><p>%s</p></div>', $string);
}


function wpify_woo_plugin_skeleton_load_textdomain() {
	load_plugin_textdomain( 'wpify-woo-plugin-skeleton', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

function wpify_woo_plugin_skeleton_load_mofile( $mofile, $domain ) {
	if ( 'wpify-woo-plugin-skeleton' === $domain && false !== strpos( $mofile, WP_LANG_DIR . '/plugins/' ) ) {
		$locale = apply_filters( 'plugin_locale', determine_locale(), $domain );
		$mofile = WP_PLUGIN_DIR . '/' . dirname( plugin_basename( __FILE__ ) ) . '/languages/' . $domain . '-' . $locale . '.mo';
	}

	return $mofile;
}

add_action( 'init', 'wpify_woo_plugin_skeleton_load_textdomain' );
add_filter( 'load_textdomain_mofile', 'wpify_woo_plugin_skeleton_load_mofile', 10, 2 );


function wpify_woo_plugin_skeleton_plugin_is_active( $plugin ) {
	if ( is_multisite() ) {
		$plugins = get_site_option('active_sitewide_plugins');
		if ( isset( $plugins[ $plugin ] ) ) {
			return true;
		}
	}

	if ( in_array( $plugin, (array) get_option( 'active_plugins', array() ), true ) ) {
		return true;
	}

	return false;
}


if ( version_compare( PHP_VERSION, WPIFY_WOO_PLUGIN_SKELETON_MIN_PHP_VERSION ) < 0 ) {
	add_action( 'admin_notices', 'wpify_woo_plugin_skeleton_php_upgrade_notice' );
} elseif ( ! wpify_woo_plugin_skeleton_plugin_is_active('woocommerce/woocommerce.php') ) {
	add_action( 'admin_notices', 'wpify_woo_plugin_skeleton_woocommerce_not_active' );
} else {
	$deps_loaded   = false;
	$vendor_loaded = false;

	$deps = array_filter( array( __DIR__ . '/deps/scoper-autoload.php', __DIR__ . '/deps/autoload.php' ), function ( $path ) {
		return file_exists( $path );
	} );

	foreach ( $deps as $dep ) {
		include_once $dep;
		$deps_loaded = true;
	}

	if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
		include_once __DIR__ . '/vendor/autoload.php';
		$vendor_loaded = true;
	}

	if ( $deps_loaded && $vendor_loaded ) {
		add_action( 'plugins_loaded', 'wpify_woo_plugin_skeleton', 11 );
		register_activation_hook( __FILE__, 'wpify_woo_plugin_skeleton_activate' );
		register_deactivation_hook( __FILE__, 'wpify_woo_plugin_skeleton_deactivate' );
		register_uninstall_hook( __FILE__, 'wpify_woo_plugin_skeleton_uninstall' );
	} else {
		add_action( 'admin_notices', 'wpify_woo_plugin_skeleton_php_vendor_missing' );
	}
}

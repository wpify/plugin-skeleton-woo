module.exports = {
	config: {
		build: 'build',
		entry: {
			'module': [
				'./src/Modules/WpifyWooPluginSkeleton/assets/js/module.js',
				'./src/Modules/WpifyWooPluginSkeleton/assets/css/module.scss',
			]
		},
	},
};

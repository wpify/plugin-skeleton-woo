=== Wpify Woo Plugin Skeleton ===
Contributors: wpify
Donate link:
Tags: WooCommerce
Requires at least: 4.0
Tested up to: 5.9
Stable tag: trunk
Requires PHP: 7.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Wpify Woo Plugin Skeleton for WooCommerce

== Description ==

Wpify Woo Plugin Skeleton for WooCommerce

Get support on https://wpify.io

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/wpify-woo-plugin-skeleton` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Use the WooCommerce -> Settings -> Wpify Woo screen to configure the plugin

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 1.0.0 =
* Initial version

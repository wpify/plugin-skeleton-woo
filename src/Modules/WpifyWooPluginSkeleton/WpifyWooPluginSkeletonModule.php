<?php

namespace WpifyWooPluginSkeleton\Modules\WpifyWooPluginSkeleton;

use WpifyWooPluginSkeleton\Plugin;
use WpifyWooPluginSkeleton\Repositories\OrderRepository;
use WpifyWooPluginSkeletonDeps\Wpify\Asset\AssetFactory;
use WpifyWooPluginSkeletonDeps\Wpify\PluginUtils\PluginUtils;
use WpifyWooPluginSkeletonDeps\Wpify\WooCore\Abstracts\AbstractModule;
use WpifyWooPluginSkeletonDeps\Wpify\WooCore\WooCommerceIntegration;

class WpifyWooPluginSkeletonModule extends AbstractModule {
	protected $requires_activation = true;

	/** @var AssetFactory */
	private $asset_factory;

	/** @var PluginUtils */
	private $utils;

	/** @var OrderRepository */
	private $order_repository;

	public function __construct(
		OrderRepository $order_repository,
		AssetFactory $asset_factory,
		PluginUtils $utils,
		WooCommerceIntegration $woocommerce_integration
	) {
		parent::__construct( $woocommerce_integration );

		$this->order_repository = $order_repository;
		$this->asset_factory    = $asset_factory;
		$this->utils            = $utils;
		$this->setup();
	}

	public function setup() {
		add_filter( 'wpify_woo_settings_' . $this->id(), array( $this, 'settings' ) );

		if ( ! $this->is_activated() ) {
			return;
		}

		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_checkout_script' ] );
	}

	/**
	 * Module ID
	 * @return string Module Id
	 */
	public function id(): string {
		return Plugin::WPIFY_WOO_PLUGIN_SKELETON_MODULE;
	}

	public function name() {
		return __( 'Wpify Woo Plugin Skeleton', 'wpify-woo-plugin-skeleton' );
	}

	/**
	 * Settings
	 * @return array[] Module settings
	 */
	public function settings(): array {
		return array(
			array(
				'label' => __( 'License', 'wpify-woo-plugin-skeleton' ),
				'desc'  => __( 'Enter your license key', 'wpify-woo-plugin-skeleton' ),
				'id'    => 'license',
				'slug'  => 'wpify-woo-plugin-skeleton',
				'type'  => 'license',
			),
			array(
				'label' => __( 'Display logo', 'wpify-woo-plugin-skeleton' ),
				'id'    => 'display_logo',
				'type'  => 'toggle',
			),
		);
	}

	/**
	 * Enqueue the frontend scripts
	 */
	public function enqueue_checkout_script() {
		if ( ! is_checkout() ) {
			return;
		}

		$this->asset_factory->wp_script( $this->utils->get_plugin_path( 'build/module.css' ) );
		$this->asset_factory->wp_script(
			$this->utils->get_plugin_path( 'build/module.js' ),
			array(
				'variables' => array(
					'wpify_woo_plugin_skeleton_module' => array(
						'language' => explode( '_', get_locale() )[0],
					),
				),
				'in_footer' => true,
			)
		);
	}
}

<?php

namespace WpifyWooPluginSkeleton\Managers;

use WpifyWooPluginSkeleton\Api\ExampleApi;

final class ApiManager {
	const PATH = 'wpify-woo-plugin-skeleton/v1';
	const NONCE_ACTION = 'wp_rest';

	public function __construct(
		ExampleApi $example_api
	) {
	}

	public function get_rest_url() {
		return rest_url( $this::PATH );
	}
}

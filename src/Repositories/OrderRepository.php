<?php

namespace WpifyWooPluginSkeleton\Repositories;

use WpifyWooPluginSkeleton\Models\OrderModel;
use WpifyWooPluginSkeletonDeps\Wpify\Model\OrderRepository as AbstractOrderRepository;

class OrderRepository extends AbstractOrderRepository {
	/**
	 * @inheritDoc
	 */
	public function model(): string {
		return OrderModel::class;
	}
}

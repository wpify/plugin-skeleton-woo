<?php

namespace WpifyWooPluginSkeleton;

use Wpify\Updates\Updates;
use WpifyWooPluginSkeleton\Managers\ApiManager;
use WpifyWooPluginSkeleton\Modules\WpifyWooPluginSkeleton\WpifyWooPluginSkeletonModule;
use WpifyWooPluginSkeletonDeps\DI\Container;
use WpifyWooPluginSkeletonDeps\Wpify\PluginUtils\PluginUtils;
use WpifyWooPluginSkeletonDeps\Wpify\WooCore\WpifyWooCore;

class Plugin {
	const WPIFY_WOO_PLUGIN_SKELETON_SLUG = 'wpify-woo-plugin-skeleton';
	const WPIFY_WOO_PLUGIN_SKELETON_MODULE = 'wpify_woo_plugin_skeleton';

	/** @var WpifyWooCore */
	private $wpify_woo_core;

	/** @var PluginUtils */
	private $plugin_utils;

	/** @var Container */
	private $container;

	public function __construct(
		WpifyWooCore $wpify_woo_core,
		PluginUtils $plugin_utils,
		Container $container,
		ApiManager $api_manager
	) {
		$this->wpify_woo_core = $wpify_woo_core;
		$this->plugin_utils   = $plugin_utils;
		$this->container      = $container;
		$this->setup();
	}

	public function setup() {
		add_filter( 'wpify_woo_modules', array( $this, 'add_module' ) );
		add_action( 'init', array( $this, 'init_updates_check' ) );

		if ( in_array( $this::WPIFY_WOO_PLUGIN_SKELETON_MODULE, $this->wpify_woo_core->get_settings()->get_enabled_modules() ) ) {
			$wpify_woo_plugin_skeleton_module = $this->container->get( WpifyWooPluginSkeletonModule::class );
			$this->wpify_woo_core->get_modules_manager()->add_module( $this::WPIFY_WOO_PLUGIN_SKELETON_MODULE, $wpify_woo_plugin_skeleton_module );
		}
	}

	public function init_updates_check() {
		$module = $this->wpify_woo_core->get_modules_manager()->get_module_by_id( $this::WPIFY_WOO_PLUGIN_SKELETON_MODULE );

		if ( ! $module ) {
			return;
		}

		$data = array(
			'license' => $module->get_setting( 'license' ),
		);

		new Updates($this->plugin_utils->get_plugin_file(), $this::WPIFY_WOO_PLUGIN_SKELETON_SLUG, $data);
	}

	public function add_module( $modules ) {
		$modules[] = array(
			'label' => __( 'Wpify Woo Plugin Skeleton', 'wpify-woo-plugin-skeleton' ),
			'value' => $this::WPIFY_WOO_PLUGIN_SKELETON_MODULE,
		);

		return $modules;
	}

	public function activate() {
	}

	public function deactivate() {
	}

	public function uninstall() {
	}

}
